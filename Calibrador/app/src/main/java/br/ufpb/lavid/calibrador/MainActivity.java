package br.ufpb.lavid.calibrador;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import br.ufpb.lavid.calibrador.serverInfo.Sender;
import wseemann.media.FFmpegMediaMetadataRetriever;


public class MainActivity extends Activity {

    private Button btnCapture;
    private Button btnStartProcess;
    private LinearLayout mImageLayout;
    private TextView mTextView;
    private ScrollView mScrollView;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private String videoPath = null;
    private Bitmap imageError;
    private int QRMethod;
    private JSONArray videoObjs;
    private JSONObject json;
    private Sender senderServer;
    private int numberFramesRecognized;
    private float totalMediaDelayedQuads;
    private float totalMediaDelayedFrames;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Seta as vari�veis UI
        //videoObjs = new ArrayList<String>();
        this.numberFramesRecognized = 0;
        this.totalMediaDelayedQuads = 0;
        this.totalMediaDelayedFrames = 0;

        videoObjs = new JSONArray();
        senderServer = new Sender();
        mTextView = (TextView) findViewById(R.id.txtQR);
        btnCapture = (Button) findViewById(R.id.btnCapture);
        btnStartProcess = (Button) findViewById(R.id.btnStartProcess); //Capturar v�deo da c�mera
        mScrollView = (ScrollView) findViewById(R.id.scrollView);
        mImageLayout = (LinearLayout) findViewById(R.id.imageLayout);

        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Intent para usar aplicativo padr�o de c�mera
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
            }
        });

        //O bot�o de processamento e scrollview s� aparecem quando o v�deo for carregado
        //btnStartProcess.setVisibility(View.GONE);
        mScrollView.setVisibility(View.GONE);

        btnStartProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                /*
                Intent intent = new Intent(getApplicationContext(),SimpleScannerActivity.class);
                startActivity(intent);
                */

                if (videoPath == null) { //Falta gravar o v�deo
                    Toast.makeText(getApplicationContext(), "Por favor grave o video primeiro", Toast.LENGTH_SHORT).show();
                } else {
                    new ProcessFrames().execute();
                }
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_zxing:
                if (checked) {
                    QRMethod = 0;
                    break;
                }
            case R.id.radio_zbar:
                if (checked) {
                    QRMethod = 1;
                    break;
                }
        }
    }

    private class ProcessFrames extends AsyncTask<Void, String, Void> {

        @Override
        protected void onPreExecute() {
            mTextView.setText("Iniciou\n");

        }

        @Override
        protected Void doInBackground(Void... params) {


            FFmpegMediaMetadataRetriever r = new FFmpegMediaMetadataRetriever();
            r.setDataSource(videoPath);
            int duration = Integer.parseInt(r.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION));
            Log.d("saida", "Duration: " + duration);
            double frameRate = Double.parseDouble(r.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_FRAMERATE));
            Log.d("saida", "FrameRate: " + frameRate);
            double increment = 1000000 / frameRate;
            Log.d("saida", "Increment: " + increment);

            publishProgress("Duration: " + duration + ". FrameRate: " + frameRate);

            for (int i = 0; i < duration * 1000; i += increment) //Deve ser frame rate
            {
                String selected;
                Bitmap bitmap = null;
                bitmap = r.getFrameAtTime(i, FFmpegMediaMetadataRetriever.OPTION_CLOSEST);
                try {
                    List<String> decodedQR;
                    if (QRMethod == 0) {
                        decodedQR = new QRCode().readQRImageZXing(bitmap);
                        selected = "ZXing ";
                    } else { //USA COMO DEFAULT
                        decodedQR = new QRCode().readQRImageZBar(bitmap);
                        selected = "Zbar ";
                    }
                    String resultToPublish = selected;
                    if (decodedQR != null) {
                        if (decodedQR.size() > 0) {
                            if (decodedQR.size() == 4) {
                                RecordInfo record = new RecordInfo(decodedQR, 4);
                                videoObjs.put(record.toJson());
                                totalMediaDelayedQuads += record.getNumberDelayedQuads();
                                totalMediaDelayedFrames += record.getTotalDelayedFrames();
                                Log.v("TESTING", "RECOGNIZED 4");
                            }
                            for (String s : decodedQR) {
                                resultToPublish += s;
                            }
                        } else {
                            resultToPublish += "erro ao decodificar";
                        }
                    }
                    publishProgress(resultToPublish);
                } catch (Exception e) {
                    Log.d("saida", e.toString());
                }
            }

            r.release();

            //Exclui o arquivo.
            File file = new File(videoPath);
            file.delete();

            Log.v("SIZE", "SIZE: " + videoObjs.length());

            totalMediaDelayedFrames /= totalMediaDelayedQuads;
            totalMediaDelayedQuads /= videoObjs.length();
            json = new JSONObject();
            try {
                json.put("total_media_delayed_frames", totalMediaDelayedFrames);
                json.put("total_media_delayed_quads", totalMediaDelayedQuads);
                json.put("frames_recognized", videoObjs.length());
                json.put("video_informations", videoObjs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("JSON", "JSONARRAY   " + json.toString());
            senderServer.send(json);
            //Codigo abaixo para n�o ficar exibindo na Galeria o v�deo exlu�do
            getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(videoPath))));






            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            mTextView.append("Terminou"); // txt.setText(result);
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onProgressUpdate(String... values) {
            mTextView.append(values[0] + "\n");
            if (values.length > 1) { //Exibe a imagem
                if (mImageLayout.getChildCount() >= 5) { //Permite add apenas 5 imagens
                    mImageLayout.removeAllViews();
                    ;
                } else {
                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
                    imageView.setScaleType(ImageView.ScaleType.CENTER);
                    imageView.setAdjustViewBounds(true);
                    imageView.setImageBitmap(imageError);
                    mImageLayout.addView(imageView);
                }

            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri vid = data.getData();
            videoPath = getRealPathFromURI(vid);
            try {
                ExifInterface exif = new ExifInterface(videoPath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                Log.d("saida", Integer.toString(orientation));
            } catch (Exception e) {
            }

            mScrollView.setVisibility(View.VISIBLE);
            Log.d("saida", videoPath);
        }


    }

    public String getRealPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            ;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    /**
     * Will remove video and file from galery
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Exclui o v�deo
        //Exclui o arquivo.
        File file = new File(videoPath);
        file.delete();
        //C�digo abaixo para n�o ficar exibindo na Galeria o v�deo exlu�do
        getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(videoPath))));
    }
}
