package br.ufpb.lavid.calibrador;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Claudio Djohnnatha on 11/06/16.
 */
public class RecordInfo {
    //String json = gson.toJson(myObj);

    private ArrayList<Integer> frames;
    private ArrayList<Integer> quadrants;
    private ArrayList<Integer> differenceBetweenFrames;
    private int mostDelayedQuadrant;
    private int numberDelayedQuads;
    private float totalDelayedFrames;
    private boolean sync;
    private JSONObject json;


    /**
     *
     * @param fullInfo informations read from QRCode contained frame number and quadrant into String
     * @param screenNumber numbers of screen analized.
     */
    public RecordInfo(List<String> fullInfo, int screenNumber){
        this.frames = new ArrayList<Integer>();
        this.quadrants = new ArrayList<Integer>();
        this.differenceBetweenFrames = new ArrayList<Integer>();
        this.sync = true;
        json = new JSONObject();
        this.numberDelayedQuads = 0;
        separateInfo(fullInfo, screenNumber);
        setMostDelayedQuadrant();
        framesDifference();
    }

    /**
     *
     *Will separate informations in string and convert to int like frame number and quadrant number.
     * @param fullInfo informations sent from qrencode
     * @param screenNumber number of screens verified in test.
     */
    public void separateInfo(List<String> fullInfo, int screenNumber){
        int frameIndex, quadIndex;
        for(int count = 0; count < screenNumber; count++){
            frameIndex = fullInfo.get(count).indexOf('f') + 1;
            quadIndex = fullInfo.get(count).indexOf('q') + 1;
            this.frames.add(Integer.parseInt(fullInfo.get(count).substring(frameIndex, quadIndex - 1)));
            this.quadrants.add(Integer.parseInt(fullInfo.get(count).substring(quadIndex, fullInfo.get(count).length())));
            //Log.v("Frames", "Number: " + this.frames.get(count) + " QUAD " + this.quadrants.get(count));
        }
    }

    /**
     * Verify if exist some difference between quadrants
     */
    public void setMostDelayedQuadrant(){
        int higher = -1;
        int quad = 0;
        this.mostDelayedQuadrant = higher;
        for(int count = 1; count < this.quadrants.size(); count++){
                if(this.frames.get(0).equals(this.frames.get(count)) == false) {
                    this.sync = false;
                }
        }
        if(sync != true) {
            for (int count = 0; count < this.quadrants.size(); count++) {
                if (higher < this.frames.get(count)) {
                    higher = this.frames.get(count);
                    quad = this.quadrants.get(count);
                }
            }
            this.mostDelayedQuadrant = quad;
        }
    }

    /**
     * Check difference among each frame and most delayedFrame.
     */
    public void framesDifference(){
        int temp;
        for(int count = 0; count < quadrants.size(); count++){
            if(sync != true) {
                temp = frames.get(this.mostDelayedQuadrant) - frames.get(count);
                this.differenceBetweenFrames.add(temp);
                if(temp != 0) {
                    this.numberDelayedQuads++;
                    this.totalDelayedFrames += temp;
                }
            }
            else{
                this.differenceBetweenFrames.add(0);
            }
        }
    }

    /**
     * @brief {Transform the object into Json structure}
     * @return Json structure containing all variables
     */

    public JSONObject toJson(){
        try {
            JSONArray arr = new JSONArray();
            this.json.put("frames", getFrames());
            this.json.put("mostDelayedQuad", getMostDelayedQuadrant());
            this.json.put("sync", isSync());
            this.json.put("number_quads_delayed", getNumberDelayedQuads());
            this.json.put("total_delayed_frames", getTotalDelayedFrames());
            this.json.put("quadrants", getQuadrants());
            this.json.put("differenceFrames", getDifferenceBetweenFrames());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }


    public ArrayList<Integer> getFrames() {
        return frames;
    }

    public void setFrames(ArrayList<Integer> frames) {
        this.frames = frames;
    }

    public ArrayList<Integer> getQuadrants() {
        return quadrants;
    }

    public void setQuadrants(ArrayList<Integer> quadrants) {
        this.quadrants = quadrants;
    }

    public int getMostDelayedQuadrant() {
        return mostDelayedQuadrant;
    }

    public void setMostDelayedQuadrant(int mostDelayedQuadrant) {
        this.mostDelayedQuadrant = mostDelayedQuadrant;
    }

    public ArrayList<Integer> getDifferenceBetweenFrames() {
        return differenceBetweenFrames;
    }

    public void setDifferenceBetweenFrames(ArrayList<Integer> differenceBetweenFrames) {
        this.differenceBetweenFrames = differenceBetweenFrames;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public JSONObject getJson() {
        return json;
    }

    public void setJson(JSONObject json) {
        this.json = json;
    }

    public int getNumberDelayedQuads() {
        return numberDelayedQuads;
    }

    public void setNumberDelayedQuads(int numberDelayedQuads) {
        this.numberDelayedQuads = numberDelayedQuads;
    }

    public float getTotalDelayedFrames() {
        return totalDelayedFrames;
    }

    public void setTotalDelayedFrames(float totalDelayedFrames) {
        this.totalDelayedFrames = totalDelayedFrames;
    }
}
