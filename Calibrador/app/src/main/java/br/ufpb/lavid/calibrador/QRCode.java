package br.ufpb.lavid.calibrador;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.QRCodeMultiReader;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Daniel on 04/07/2015.
 */
public class QRCode {

    /*
     * Retorna uma lista de Strings, com o conte�do dos QR Codes decodificados
     */
    public List<String> readQRImageZBar(Bitmap bMap) throws NotFoundException, FormatException, ChecksumException {

        List<String> decodedQR = new ArrayList<String>();
        try {
            ImageScanner s = new ImageScanner();

            /*� necess�rio converter o bitmap para escala cinza*/
            int width = bMap.getWidth();
            int height = bMap.getHeight();
            int[] pixels = new int[width * height];
            bMap.getPixels(pixels, 0, width, 0, 0, width, height);
            Image barcode = new Image(width, height, "RGB4");
            barcode.setData(pixels);
            int result = s.scanImage(barcode.convert("Y800"));

            if (result != 0) {

                for (Symbol symbol : s.getResults()) {
                    // .getData() gives the string content of the QR code
                    decodedQR.add(symbol.getData());
                }
            } else {
                Log.d("saidasaida", "sem resultados");
            }
        } catch (Exception e) {
            Log.d("saidasaida", e.toString());
        }

        return decodedQR;
    }

    /*
   * Retorna uma lista de Strings, com o conte�do dos QR Codes decodificados
   */
    public List<String> readQRImageZXing(Bitmap bMap) throws NotFoundException, FormatException, ChecksumException {

        Log.d("conteudo", "tentando decodificar");

        int width = bMap.getWidth(), height = bMap.getHeight();
        int[] pixels = new int[width * height];
        bMap.getPixels(pixels, 0, width, 0, 0, width, height);

        RGBLuminanceSource source = new RGBLuminanceSource(width, height, pixels);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        List<String> decodedQR = new ArrayList<String>();
        QRCodeMultiReader reader = new QRCodeMultiReader();

        //Hashtable<DecodeHintType, Object> decodeHints = new Hashtable<DecodeHintType, Object>();
        //decodeHints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);

        Result[] resultSet = reader.decodeMultiple(bitmap);

        for (Result r : resultSet) {
            decodedQR.add(r.getText());
        }

        return decodedQR;
    }

}